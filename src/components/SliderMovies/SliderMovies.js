import React from 'react';
import { Carousel, Button } from 'antd';
import { Link } from 'react-router-dom';
import Loading from '../Loading';

import './SliderMovies.scss';

export default function SliderMovies(props) {
  //get movies from father component for props
  const { movies } = props;

  //validate loding true or movies result is null
  if (movies.loading || !movies.result) {
    return <Loading />;
  }

  //set movies
  const { results } = movies.result;

  return (
    //iterate for each movie and create images for slider
    <Carousel autoplay className='slider-movies'>
      {results.map(movie => (
        <Movie key={movie.id} movie={movie} />
      ))}
    </Carousel>
  );
}

//component movie
function Movie(props) {
  //desctructuring de movie from SliderMovies(father component)
  const {
    movie: { id, backdrop_path, title, overview }
  } = props;
  //set image url
  const backdropPath = `https://image.tmdb.org/t/p/original${backdrop_path}`;

  return (
    //set data movie: url, title, overview and background image from API
    <div
      className='slider-movies__movie'
      style={{
        backgroundImage: `url('${backdropPath}')`
      }}
    >
      <div className='slider-movies__movie-info'>
        <div>
          <h2>{title}</h2>
          <p>{overview}</p>
          <Link to={`/movie/${id}`}>
            <Button type='primary'>Ver más</Button>
          </Link>
        </div>
      </div>
    </div>
  );
}
