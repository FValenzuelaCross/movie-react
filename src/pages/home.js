import React from 'react';
import { Row, Col } from 'antd';
import useFectch from '../hooks/useFetch';
import { URL_API, API } from '../utils/contants';

import SliderMovies from '../components/SliderMovies';
import MovieList from '../components/MovieList';

export default function Home() {
  const newMovies = useFectch(
    `${URL_API}/movie/now_playing?api_key=${API}&language=es-ES&page=1`
  );
  const popularMovies = useFectch(
    `${URL_API}/movie/popular?api_key=${API}&language=es-ES&page=1`
  );
  const topRatedMovies = useFectch(
    `${URL_API}/movie/top_rated?api_key=${API}&language=es-ES&page=1`
  );
  return (
    <Row>
      <SliderMovies movies={newMovies} />
      <Col span={12}>
        <MovieList title='Peliculas populares' movies={popularMovies} />
      </Col>
      <Col span={12}>
        <MovieList
          title='Top mejores peliculas rankeadas'
          movies={topRatedMovies}
        />
      </Col>
    </Row>
  );
}
